const jwt = require("jsonwebtoken");
const secret = "merng_assetmgt";

module.exports.verify = token => {
	return jwt.verify(token, secret, function(err, decoded){
		if(err) {
			return null;
		} else {
			return decoded;
		}
	})
}

