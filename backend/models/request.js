//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const requestSchema = new Schema({
	userId: {
		type: String,
		required: true
	},
	assetId: {
		type: String,
		required: true
	},
	dateRequested: {
		type: String,
		default: Date.now
	},
	dateApprovedOrDenied: {
		type: String,
		default: "Pending"
	},
	isApproved: {
		type: Boolean,
		default: false
	},
	approverId: {
		type: String,
		default: "Pending"
	}, 
	dateReturned: {
		type: String,
		default: "Pending"
	}
})

//export schema as model
module.exports = mongoose.model("Request", requestSchema);