//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const userSchema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	email: {
		type: String,
		required: true,
		unique: true,
		minlength: 5
	},
	password: {
		type: String,
		required: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("User", userSchema);