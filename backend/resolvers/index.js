const Asset = require("../models/asset");
const Category = require("../models/category");
const Request = require("../models/request");
const Log = require("../models/log");
const User = require("../models/user");

//dependency
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");
const validToken = require("../jwt-verify");

const resolvers = {
	Category: {
		assets: ({ _id }, args)=>{
			// console.log(categoryId); //undefined
			// console.log(parent)
			return Asset.find({categoryId: _id})
		}
	},
	Asset: {
		category: ({ categoryId}, args) =>{
			return Category.findById(categoryId)
		},
		requests: ({ _id }, args)=>{
			return Request.find({assetId: _id})
		}
	},
	Log: {
		user: ({ userId}, args) => {
			return User.findById(userId)
		}
	},
	Request: {
		user: ({ userId}, args) => {
			return User.findById(userId)
		},
		asset: ({ assetId}, args)=> {
			return Asset.findById(assetId)
		},
		approver: ({approverId}, args)=>{
			return User.findById(approverId)
		}
	},
	User: {
		logs: ({ _id }, args)=>{
			return Log.find({userId: _id})
		}, 
		requests: ({ _id }, args)=>{
			return Request.find({userId: _id})
		},
		approves: ({_id})=>{
			return User.find({userId: _id})
		}
	},
	Query: {
		assets: ()=> {
			return Asset.find({})
		},
		categories: ()=> {
			return Category.find({})
		},
		logs: ()=> {
			return Log.find({})
		},
		requests: ()=> {
			return Request.find({})
		},
		users: () =>{
			return User.find({})
		},
		asset: (parent, {id}) =>{
			return Asset.findById(id)
		},
		category: (parent, {id}) =>{
			return Category.findById(id)
		},
		log: (parent, {id}) =>{
			return Log.findById(id)
		},
		request: (parent, {id}) =>{
			return Request.findById(id)
		},
		user: (parent, {id}) =>{
			return User.findById(id)
		}
	},
	Mutation:{
		storeAsset: (parent, {name, description, categoryId}) => {

			//validation

			let asset = new Asset({
				name, description, categoryId
			})
			return asset.save()
		},
		storeRequest: (parent, { userId, assetId, dateRequested}) => {

			//validation
			//verify and decode token
			//check if registered user 
			//check if asset exists

			let request = new Request({
				userId, assetId, dateRequested
			})
			return request.save()
		},
		//hashing of password
		registerUser: (parent, {name, email, password}) => {

			//hash
			let user = new User({
				name, 
				email, 
				password : bcrypt.hashSync(password, 8)
			})

			return user.save().then((user, err) => {
				return err ? false : true;
			})
		},
		//password needs to be unhashed
		//login token needs to be created
		loginUser: (parent, { email, password}) => {

			//validation
			let query = User.findOne({ email });
			return query.then(user => {
				if(user === null) {
					return null
				} 

				//unhash password
				let isPasswordMatched = bcrypt.compareSync(password, user.password);

				//create login token
				if(isPasswordMatched){
					user.token = auth.createToken(user.toObject());
					return user;
				} else {
					return null;
				}
			})
		},
		updateRequestStatus: (parent, { id, isApproved, dateApproved, token}) => {

			//verify and decode a token
			console.log(validToken.verify(token))

			
			if(isValidToken && isValidToken.isAdmin){
				console.log("valid and isAdmin");
				//check if registered user and isAdmin
				return User.exists({_id: isValidToken._id})
					.then(()=>{
						let updates = {
							isApproved,
							approverId: isValidToken._id,
							dateApprovedOrDenied: new Date()
						}
						return Request.findByIdAndUpdate(id, {isApproved, dateApproved, approverId}, { new: true })

					})
					.catch(error=>{
						console.log(error);
						return null;
					})
			} else {
				console.log("invalid token or not admin");
				return null;
			}

			//validations
			
		},
		updateDateReturned: (parent, {id, dateReturned}) => {

			//validations
			//check if token is valid
			//heck if user exists and isAdmin

			return Request.findByIdAndUpdate(id, { dateReturned}, {new: true})
		}
	}

}

module.exports = resolvers;