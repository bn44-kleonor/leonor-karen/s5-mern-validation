import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Navbar } from 'react-bulma-components';

const AppNavbar = props => {
	console.log(props);
	let endNav = "";
	let startNav = "";

	if(props.token) {
		endNav = (
			<Fragment>
				<Link className="navbar-item has-text-white" to="/dashboard">Welcome, {props.name}!</Link>
			    <Link className="navbar-item" to="/logout">Logout</Link>
			</Fragment>
		)
		startNav = (
			<Fragment>
	          <Link className="navbar-item" to="/assets">Assets</Link>
	          <Link className="navbar-item" to="/requests">Requests</Link>
	        </Fragment>
		)
	} else {
		endNav = (
			<Fragment>
            	<Link className="navbar-item" to="/login">Login</Link>
            	<Link className="navbar-item" to="/register">Register</Link>
			</Fragment>
		)
	}
	if(props.token && props.isAdmin) {
		startNav = (
			<Fragment>
			  <Navbar.Item>Users</Navbar.Item>
	          <Navbar.Item>Categories</Navbar.Item>
			</Fragment>
		)
	}

	return (
		<Navbar className='is-black'>
	      <Navbar.Brand>
	        <Link className="navbar-item" to="/">
	        	<strong>MERN AM</strong>
	        </Link>
	        <Navbar.Burger />
	      </Navbar.Brand>
	      <Navbar.Menu>
			<Navbar.Container>
				{ startNav }
			</Navbar.Container>
	        <Navbar.Container position='end'>
	          { endNav }
	        </Navbar.Container>
	      </Navbar.Menu>
	    </Navbar>

	)
}

export default AppNavbar;