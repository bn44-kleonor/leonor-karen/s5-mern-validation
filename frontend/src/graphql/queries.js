import { gql } from 'apollo-boost';

const getAssetsQuery = gql`
	{
		assets{
			id
			name
			description
			categoryId
			category {
				name
			}
		}
	}
`;

const getAssetQuery = gql`
	query ($id: ID!) {
		asset(id: $id){
			id
			name
			description
			categoryId
			category {
				name
			}
		}
	}
`

export { getAssetsQuery, getAssetQuery };