import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

//pages
// import AppNavbar from './partials/AppNavbar';
// import AssetPage from './pages/AssetPage';
// import RequestPage from './pages/RequestPage';
// import NotFoundPage from './pages/NotFoundPage';
// import AssetShow from './components/show/AssetShow';
// import RegisterPage from './pages/RegisterPage';
// import LoginPage from './pages/LoginPage';
// import DashboardPage from './pages/DashboardPage';

//imported pages
import App from './app';

// apollo client
const client = new ApolloClient({ uri: 'http://localhost:8080/graphql'});

const root = document.querySelector("#root");
const pageComponent = (
	<ApolloProvider  client={client}>
		<App />
	</ApolloProvider>
);

ReactDOM.render(pageComponent, root);