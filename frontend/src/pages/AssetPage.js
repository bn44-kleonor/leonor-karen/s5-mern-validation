import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';
import { graphql } from 'react-apollo'; //to pass query as props

import AssetAdd from '../components/forms/AssetAdd';
import AssetList from '../components/lists/AssetList';

import { getAssetsQuery } from '../graphql/queries';

const AssetPage = props => {
	// console.log(props.data.assets);

	const data = props.data;

	return (
		<Section size='medium'>
	      <Heading>Assets</Heading>
	      <Columns>
	        <Columns.Column>
	          <AssetAdd />
	        </Columns.Column>
	        <Columns.Column>
	          <AssetList assets={data.assets}/>
	        </Columns.Column>
	      </Columns>
	    </Section>
	)
}

export default graphql(getAssetsQuery)(AssetPage);