import React from 'react'

import 'react-bulma-components/dist/react-bulma-components.min.css'
import { 
    Heading,
    Section
} from 'react-bulma-components'

import { Link } from 'react-router-dom';

const DashboardPage= (props) => {
	// console.log(props.currentUser().isAdmin);

	const isAdmin = props.currentUser().isAdmin;

	const btnUsers = <Link to="users" className="button is-primary">Users</Link>

	return (
		<Section className="sectionStyle">
			<Heading>Dashboard</Heading>
			{ isAdmin ? btnUsers: "" }
		</Section>
	)
}

export default DashboardPage;
