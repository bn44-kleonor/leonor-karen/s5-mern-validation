import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import verifyToken from './jwt-verify';

//pages
import AppNavbar from './partials/AppNavbar';
import AssetPage from './pages/AssetPage';
import RequestPage from './pages/RequestPage';
import NotFoundPage from './pages/NotFoundPage';
import AssetShow from './components/show/AssetShow';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import DashboardPage from './pages/DashboardPage';

//auth route
const AuthRoute = ({token, ...props}) => {
	return token ? <Route {...props} /> : <Redirect to="/login" />
}

//admin route
const AdminRoute = ({token, isAdmin, ...props}) => {
	//if token and admin
	//if token
	//if wala lahat
	if(token && isAdmin){
		return <AuthRoute {...props} token={token} />
	} else if(token && !isAdmin) {
		return <Redirect to="/dashboard" />
	} else {
		return <Redirect to="/login" />
	}
}

const App = () => {
	//states
	// console.log(localStorage.getItem("token"));
	const [ token, setToken ] = useState(localStorage.getItem("token"));
	// console.log(jwt.decode(token, {complete: true}));
	// console.log(verifyToken(token));
	const decoded = verifyToken(token);
	console.log(decoded);
	const [isAdmin, setIsAdmin] = useState(decoded ? decoded.isAdmin : null);
	const [name, setName] = useState(decoded ? decoded.name : null);

	//update session in login and logout
	const updateSession = () => {
		setToken(token);
		setIsAdmin(decoded.isAdmin);
		setName(decoded.name);
	}

	const Login = (props) => <LoginPage {...props} token={token} updateSession={updateSession} />

	const Logout = (props) => {
		localStorage.clear();
		updateSession();
		window.location = "/login";
	}

	const currentUser = () => {
		return { name, isAdmin, token }
	}

	const Dashboard = (props) => <DashboardPage {...props} currentUser={currentUser} updateSession />

	return (
		<BrowserRouter>
			<AppNavbar name={name} isAdmin={isAdmin} token={token}/>
			<Switch>
				<Route exact path="/" />
				{/*<AdminRoute token={token} isAdmin={isAdmin} component={UserPage} exact path="/users "/>*/}
				<Route component={AssetPage} exact path="/assets"/>
				<Route component={AssetShow} exact path='/asset/:id' />
				<Route component={RequestPage} exact path="/requests"/>
				<Route component={RegisterPage} exact path="/register"/>
				<Route component={Login} path="/login"/>
				<Route component={Logout} path="/logout" />
				<AuthRoute token={token} component={Dashboard} path="/dashboard"/>
				<Route component={NotFoundPage} />
			</Switch>
		</BrowserRouter>
	)
}

export default App;
