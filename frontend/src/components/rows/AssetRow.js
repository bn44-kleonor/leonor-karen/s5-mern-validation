import React from 'react';
import { Link } from 'react-router-dom';

const AssetRow = props => {
  // console.log(props)
  const asset = props.asset;
	return(
		<tr>
            <td>{asset.name}</td>
            <td>{asset.description}</td>
            <td>{asset.category.name}</td>
            <td>
              <button className='button is-danger'>Remove</button>
              &nbsp;
              <button className='button is-warning'>Update</button>
              &nbsp;
              <Link to={`/asset/${asset.id}`}>
                <button className='button is-primary'>View</button>
              </Link>
            </td>
          </tr>
	)
}

export default AssetRow;