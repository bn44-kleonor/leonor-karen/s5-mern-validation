import React from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';


const AssetAdd = props => {
	return (
		<Card>
            <Card.Header>
              <Card.Header.Title>Add Asset</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <form>
                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Asset Name
                  </label>
                  <div className='control'>
                    <input className='input' type='text' required />
                  </div>
                </div>
                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Category
                  </label>
                  <div className='control'>
                    <div className='select is-fullwidth'>
                      <select required defaultValue={'DEFAULT'}>
                        <option value='DEFAULT' disabled>
                          Select Category
                        </option>
                        <option>Category 1</option>
                      </select>
                    </div>
                  </div>
                </div>
                <br />
                <div className='field'>
                  <div className='control'>
                    <button type='submit' className='button is-link is-success'>
                      Add
                    </button>
                  </div>
                </div>
              </form>
            </Card.Content>
          </Card>

	)
}

export default AssetAdd;