import jwt from 'jsonwebtoken';
const secret = "merng_assetmgt";

const verifyToken = token => {
	return jwt.verify(token, secret, function(err, decoded){
		if(err){
			// console.log(err);
			return null;
		} else {
			// console.log(decoded);
			return decoded;
		}
	})
}

export default verifyToken;
